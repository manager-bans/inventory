from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('course/', views.listCourses, name='course'),
    path('constitution/', views.listConstitution, name='constitution'),
    path('lecturer/', views.listLecturers, name='lecturer'),
    path('artdisp/<str:pk>/', views.detailedArticlesdisp, name='artdisp'),
    path('executive/<str:pk>/', views.detailedExecutive, name='executive'),
    path('aluwel/', views.listAluwel, name='aluwel'),
    path('alumini/', views.listAlumini, name='alumini'),
    path('gallery/<str:pk>/', views.detailedGallery, name='gallery'),
    path('gallery_group/<str:pk>/', views.listGallerygroup, name='gallery_group'),
    path('spotlight/', views.listSpotlight, name='spotlight'),
    path('office/', views.listOffice, name='office'),
    path('inspire', views.listInspire, name='inspire'),
    path('article/<str:pk>/', views.detailedArticle, name='article'),
    path('art_group/', views.listArticlegroup, name='art_group'),
    path('alu_const/', views.listAluConstitution, name='alu_const'),
    path('event/', views.listEvent, name='event'),
    path('exec_arc', views.listExecutiveArc, name='exec_arc')

]
