# Generated by Django 3.2.6 on 2021-08-16 23:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apis', '0004_auto_20210804_2234'),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('type', models.CharField(blank=True, max_length=100)),
                ('date', models.DateField(blank=True)),
            ],
        ),
        migrations.AlterField(
            model_name='gallery',
            name='categoryId',
            field=models.IntegerField(verbose_name='category id'),
        ),
    ]
