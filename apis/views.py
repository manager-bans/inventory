from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from apis.serializers import CourseSerializers, ConstitutionSerializers, LecturerSerializers, ArticlesdispSerializers, \
    ExecutiveSerializers, AluminiSerializers, AluwelSerializers, GallerySerializers, GallerygroupSerializers, \
    SpotlightSerializers, OfficeSerializers, InspireSerializers, ArticleSerializers, ArticlegroupSerializers, \
    AluConstitutionSerializers, ExecutiveArcSerializers, EventSerializers
from apis.models import Course, Constitution, Lecturer, Articlesdisp, Executive, Alumini, Aluwel, Gallery, Gallerygroup, \
    Spotlight, Office, Inspire, Article, Articlegroup, AluConstitution, ExecutiveArc, Event


# Create your views here.
def home(request):
    return render(request, 'index.html')


@api_view(['GET'])
def listCourses(request):
    course = Course.objects.all()
    serializer = CourseSerializers(course, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listConstitution(request):
    constitution = Constitution.objects.all()
    serializer = ConstitutionSerializers(constitution, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listLecturers(request):
    lecturer = Lecturer.objects.all()
    serializer = LecturerSerializers(lecturer, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def detailedArticlesdisp(request, pk):
    artdisp = Articlesdisp.objects.get(id=pk)
    serializer = ArticlesdispSerializers(artdisp, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def detailedExecutive(request, pk):
    executive = Executive.objects.get(id=pk)
    serializer = ExecutiveSerializers(executive, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def listAluwel(request):
    aluwel = Aluwel.objects.all()
    serializer = AluwelSerializers(aluwel, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listAlumini(request):
    alumini = Alumini.objects.all()
    serializer = AluminiSerializers(alumini, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def detailedGallery(request, pk):
    gallery = Gallery.objects.get(id=pk)
    serializer = GallerySerializers(gallery, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def listGallerygroup(request):
    gallery_group = Gallerygroup.objects.all()
    serializer = GallerygroupSerializers(gallery_group, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listSpotlight(request):
    spotlight = Spotlight.objects.all()
    serializer = SpotlightSerializers(spotlight, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listOffice(request):
    office = Office.objects.all()
    serializer = OfficeSerializers(office, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listInspire(request):
    inspire = Inspire.objects.all()
    serializer = InspireSerializers(inspire, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def detailedArticle(request, pk):
    article = Article.objects.get(id=pk)
    serializer = ArticleSerializers(article, many=False)
    return Response(serializer.data)


@api_view(['GET'])
def listArticlegroup(request):
    art_group = Articlegroup.objects.all()
    serializer = ArticlegroupSerializers(art_group, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listAluConstitution(request):
    alu_const = AluConstitution.objects.all()
    serializer = AluConstitutionSerializers(alu_const, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listEvent(request):
    event = Event.objects.all()
    serializer = EventSerializers(event, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def listExecutiveArc(request):
    exec_arc = ExecutiveArc.objects.all()
    serializer = ExecutiveArcSerializers(exec_arc, many=True)
    return Response(serializer.data)
