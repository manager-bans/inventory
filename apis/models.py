from django.db import models


# Create your models here.


class Event(models.Model):
    id = models.AutoField(primary_key=True)
    eventName = models.CharField(max_length=200, help_text='The name of the event', verbose_name='event name')
    eventDate = models.DateField(verbose_name='event date', null=True)
    place = models.CharField(max_length=100, blank=True)
    piclocation = models.CharField(max_length=200, verbose_name='picture location', blank=True)
    info = models.TextField(verbose_name='information', blank=True)

    def __str__(self):
        return self.eventName


class Constitution(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    chapter = models.CharField(max_length=50, help_text='The chapter of the constitution')
    info = models.TextField(blank=True)

    def __str__(self):
        return ' chapter ' + self.chapter + ' of ' + self.title


class Office(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, help_text='office name', )
    message = models.TextField(blank=True)
    piclocation = models.CharField(max_length=200, blank=True, verbose_name='picture location')

    def __str__(self):
        return self.name


class Job(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    type = models.CharField(max_length=100, blank=True)
    date = models.DateField(blank=True)
