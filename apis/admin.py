from django.contrib import admin
from .models import Course,Articlesdisp,Executive,Alumini,Aluwel,Lecturer,Event,Gallery,Gallerygroup,Spotlight,Constitution,Office,Inspire,Article,Articlegroup,AluConstitution,ExecutiveArc,Job

# Register your models here.
admin.site.register(Course)
admin.site.register(Articlesdisp)
admin.site.register(Executive)
admin.site.register(Alumini)
admin.site.register(Aluwel)
admin.site.register(Lecturer)
admin.site.register(Event)
admin.site.register(Gallery)
admin.site.register(Gallerygroup)
admin.site.register(Spotlight)
admin.site.register(Constitution)
admin.site.register(Office)
admin.site.register(Inspire)
admin.site.register(Article)
admin.site.register(Articlegroup)
admin.site.register(AluConstitution)
admin.site.register(ExecutiveArc)
admin.site.register(Job)



