from rest_framework import serializers
from apis.models import Course,Constitution,Lecturer,Articlesdisp,Executive,Alumini,Aluwel,Gallery,Gallerygroup,Spotlight,Office,Inspire,Article,Articlegroup,AluConstitution,ExecutiveArc,Event

class CourseSerializers(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'

class ConstitutionSerializers(serializers.ModelSerializer):
    class Meta:
        model = Constitution
        fields = '__all__'

class LecturerSerializers(serializers.ModelSerializer):
    class Meta:
        model = Lecturer
        fields = '__all__'

class ExecutiveArcSerializers(serializers.ModelSerializer):
    class Meta:
        model = ExecutiveArc
        fields = '__all__'

class ArticlesdispSerializers(serializers.ModelSerializer):
    class Meta:
        model = Articlesdisp
        fields = '__all__'

class AluminiSerializers(serializers.ModelSerializer):
    class Meta:
        model = Alumini
        fields = '__all__'

class AluwelSerializers(serializers.ModelSerializer):
    class Meta:
        model = Aluwel
        fields = '__all__'

class GallerySerializers(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = '__all__'

class GallerygroupSerializers(serializers.ModelSerializer):
    class Meta:
        model = Gallerygroup
        fields = '__all__'

class SpotlightSerializers(serializers.ModelSerializer):
    class Meta:
        model = Spotlight
        fields = '__all__'

class OfficeSerializers(serializers.ModelSerializer):
    class Meta:
        model = Office
        fields = '__all__'

class InspireSerializers(serializers.ModelSerializer):
    class Meta:
        model = Inspire
        fields = '__all__'

class ArticleSerializers(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'


class ArticlegroupSerializers(serializers.ModelSerializer):
    class Meta:
        model = Articlegroup
        fields = '__all__'

class AluConstitutionSerializers(serializers.ModelSerializer):
    class Meta:
        model = AluConstitution
        fields = '__all__'

class ExecutiveSerializers(serializers.ModelSerializer):
    class Meta:
        model = Executive
        fields = '__all__'

class EventSerializers(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'




